## Description

This project is attempt to implement [c@ard](https://www.tindie.com/products/Russtopia/crdtm-password-generatorrecall-card/) functionality as a [simple web application](https://iusenko.gitlab.io/total-recall).

## Tips

* `npx yarn-check` to find unsued packages(here are [details](https://www.npmjs.com/package/yarn-check))
* `yarn upgrade-interactive --latest` upgrade all project dependencies in interactive mode

## Resources

* [Hero Patterns](https://www.heropatterns.com/)
* [Tailwindcss](https://tailwindcss.com/)
* [Heroicons UI](https://github.com/sschoger/heroicons-ui)
* [C@RD Mark II (Credential @ccess Recall Device)](https://www.tindie.com/products/Russtopia/crdtm-mark-ii-credential-ccess-recall-device/)
* [Building a distributed eventually consistent key-value store with DNS and Elixir](https://blog.dnsimple.com/2017/12/fun-with-dns/)

<!-- * [Svelte + Parcel boilerplate](https://github.com/RedHatter/svelte-parcel-example)
* [How to use React Context effectively](https://kentcdodds.com/blog/how-to-use-react-context-effectively) -->


