export const DEFAULT_OPTIONS = Object.freeze({
  /**
   * Include special character(s).
   * List of characters: @#$%^&*+-
   */
  specials: false,

  /**
   * Includes number(s).
   */
  numbers: true,

  /**
   * Includes lower case character(s).
   */
  lowercase: true,

  /**
   * Includes upper case character(s).
   */
  uppercase: true,

  /**
   * Length of the password.
   */
  length: 12
})

export const isString = s => typeof s === 'string' || s instanceof String

export const getInputCharTable = options => {
  return [
    options.specials && '!@#$%^&*-_?~+=',
    options.numbers && '0123456789',
    options.lowercase && 'abcdefghijklmnopqrstuvwxyz',
    options.uppercase && 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  ]
    .filter(Boolean)
    .join('')
}

export const getCharHash = (key, index, subindex) => {
  const c = index >= key.length ? key.charCodeAt(index % key.length) : key.charCodeAt(index)
  return (c * 3 + +index * 11 + key.length * 17) * (7 + subindex * 3)
}

export const getCharByHash = (hash, table) => {
  const index = hash % table.length
  return table[index]
}

export const die = msg => {
  throw new Error(msg)
}

// TODO: implement me
export const checkOptions = ({ length }) => true

/**
 * Generate password base on input parameters and given options.
 *
 * @param {String} key
 * @param {Number} index
 * @param {Object} options
 *
 * @see #DEFAULT_OPTIONS
 *
 */
export const generate = (key, subindex, options) => {
  const table = getInputCharTable(options)
  return [...Array(options.length).keys()]
    .map(index => getCharHash(key, index, subindex))
    .map(hash => getCharByHash(hash, table))
    .join('')
}

export default (key, n = 1, options = DEFAULT_OPTIONS) => {
  if (!isString(key) || key.trim() === '') return []
  if (n <= 0) return []
  if (!checkOptions(options)) return []

  return [...new Array(n).keys()].map(i => generate(key, i, options))
}
