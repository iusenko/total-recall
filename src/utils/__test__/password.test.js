import { generate } from '../password'
import { getCharHash } from '../password'
import { getInputCharTable } from '../password'
import { DEFAULT_OPTIONS as o } from '../password'

describe('password', () => {
  it('generate', () => {
    const key = 'master-key'
    expect(generate(key, 0, o)).toEqual('7iDdn1T8lmxI')
    expect(generate(key, 1, o)).toEqual('a8CSosQCuWUS')
    expect(generate(key, 2, o)).toEqual('dYBxpTN6Dwh2')
    expect(generate(key, 3, o)).toEqual('gOAcqkKAM6Ec')
    expect(generate(key, 4, o)).toEqual('jEzRrLH4VG1m')

    expect(generate(key, 0, { ...o, specials: true })).toEqual('JmlHx3=aB2Tw')
    expect(generate(key, 1, { ...o, specials: true })).toEqual('g-Ao#wMGM+Qu')
    expect(generate(key, 2, { ...o, specials: true })).toEqual('@GP5jZl+X-Ns')
    expect(generate(key, 3, { ...o, specials: true })).toEqual('ye%!Oe-u-%Kq')
    expect(generate(key, 4, { ...o, specials: true })).toEqual('5!5H5HH!5!Ho')

    for (let i = 0; i < 25; i++) {
      expect(generate(key, i, o)).toEqual(generate(key, i, o))
    }
  })

  it('getInputCharTable', () => {
    expect(getInputCharTable({ specials: true })).toEqual('!@#$%^&*-_?~+=')
    expect(getInputCharTable({ specials: true, numbers: true })).toEqual('!@#$%^&*-_?~+=0123456789')
    expect(getInputCharTable({ specials: true, numbers: true, uppercase: true })).toEqual(
      '!@#$%^&*-_?~+=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    )
    expect(getInputCharTable({ specials: true, numbers: true, lowercase: true, uppercase: true })).toEqual(
      '!@#$%^&*-_?~+=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    )
    expect(getInputCharTable(o)).toEqual('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
  })

  it('getCharHash', () => {
    expect(getCharHash('abc', 0, 0)).toEqual(2394)
    expect(getCharHash('abc', 0, 1)).toEqual(3420)
    expect(getCharHash('abc', 1, 1)).toEqual(3560)

    expect(getCharHash('abc', 1, 0)).toEqual(2492)
    expect(getCharHash('abc', 0, 2)).toEqual(4446)

    expect(getCharHash('abc', 11, 0)).toEqual(3283)
    expect(getCharHash('abc', 12, 11)).toEqual(18960)
  })
})
