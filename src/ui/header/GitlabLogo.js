import React from 'react'

export const GitlabLogo = () =>
    <a href='https://gitlab.com/iusenko/total-recall'
        className='opacity-50'>
        <svg viewBox="0 0 48 48" width="24" height="24">
            <g id="surface1">
                <path style={{ fill: '#E53935' }} d="M 24 43 L 16 20 L 32 20 Z " />
                <path style={{ fill: '#FF7043' }} d="M 24 43 L 42 20 L 32 20 Z " />
                <path style={{ fill: '#E53935' }} d="M 37 5 L 42 20 L 32 20 Z " />
                <path style={{ fill: '#FFA726' }} d="M 24 43 L 42 20 L 45 28 Z " />
                <path style={{ fill: '#FF7043' }} d="M 24 43 L 6 20 L 16 20 Z " />
                <path style={{ fill: '#E53935' }} d="M 11 5 L 6 20 L 16 20 Z " />
                <path style={{ fill: '#FFA726' }} d="M 24 43 L 6 20 L 3 28 Z " />
            </g>
        </svg>
    </a>

export default GitlabLogo