import React from 'react'

import KeyIcon from './KeyIcon'
import KeyInput from './KeyInput'
import ClearKeyIcon from './ClearKeyIcon'
import GitlabLogo from './GitlabLogo'
import DisableAoutoComplete from './DisableAoutoComplete'

import './Header.css'

export const MasterKey = () =>
    <span className='relative inline-block mx-10'>
        <KeyIcon />
        <DisableAoutoComplete />
        <KeyInput />
        <ClearKeyIcon />
    </span>

export const Header = () => {
    const className = [
        'h-16',
        'flex items-center justify-center',
        'bg-white border-b border-gray-200',
        'fixed top-0 inset-x-0 z-100',
    ].join(' ')

    return <div id='header' className={className}>
        <GitlabLogo />
        <MasterKey />
        <GitlabLogo />
    </div>
}

export default Header