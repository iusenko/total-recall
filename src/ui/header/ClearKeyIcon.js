import React from 'react'
import { useKey } from '@context'

export const ClearKeyIcon = () => {
    const { key, clear } = useKey()
    const noIcon = !key || key.length == 0
    if (noIcon) return <div />

    const className = [
        'flex items-center',
        'absolute inset-y-0 right-0 pr-4',
        'cursor-pointer',
        'z-50'
    ].join(' ')

    return <div className={className} onClick={clear}>
        <svg className='fill-current text-gray-600 w-4 h-4' viewBox="0 0 24 24">
            <path d="M4.93 19.07A10 10 0 1 1 19.07 4.93 10 10 0 0 1 4.93 19.07zm1.41-1.41A8 8 0 1 0 17.66 6.34 8 8 0 0 0 6.34 17.66zM13.41 12l1.42 1.41a1 1 0 1 1-1.42 1.42L12 13.4l-1.41 1.42a1 1 0 1 1-1.42-1.42L10.6 12l-1.42-1.41a1 1 0 1 1 1.42-1.42L12 10.6l1.41-1.42a1 1 0 1 1 1.42 1.42L13.4 12z" />
        </svg>
    </div>
}

export default ClearKeyIcon