import React from 'react'

/**
 * Fake and non visible input required to disable automcompletion
 * in Chrome browser.
 * 
 * For some reasons automcomplet="off" does not work in the
 * Chrome. The most common is to add a hidden field 
 * above the field you would like to remove autocomplete 
 * from that will basically grab that autofill value before 
 * it can hit the real input.
 * 
 * @see https://www.20spokes.com/blog/what-to-do-when-chrome-ignores-autocomplete-off-on-your-form
 */
export const DisableAoutoComplete = () =>
    <input style={{ display: 'none' }} type='password' name='fakepasswordremembered' />

export default DisableAoutoComplete