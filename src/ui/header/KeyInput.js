import React from 'react'
import { useKey } from '@context'

export const KeyInput = () => {
  const { key, setKey, clear } = useKey()

  const onChange = e => setKey(e.target.value)
  const onEsc = ({ key }) => key === 'Escape' && clear()

  const className = [
    'key-input',
    'w-full py-2 px-10 rounded-lg',
    'bg-gray-200 text-gray-700',
    'border border-gray-200 ',
    'focus:outline-none focus:bg-white focus:border-gray-300',
    'appearance-none leading-tight',
    'placeholder-gray-900'
  ].join(' ')
  const hint = 'Enter master key'

  return (
    <input
      className={className}
      onChange={onChange}
      value={key}
      placeholder={hint}
      style={{ width: 320 }}
      onKeyDown={onEsc}
    />
  )
}

export default KeyInput
