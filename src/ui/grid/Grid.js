import React, { useMemo } from 'react'
import { useRef } from 'react'
import { useKey } from '@context'
import { generatePasswords } from '@utils'
import './Grid.css'

const LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const COLORS = ['purple', 'red', 'indigo', 'green', 'orange', 'blue']

export const getColor = index => index >= COLORS.length
    ? COLORS[index % COLORS.length]
    : COLORS[index]

export const copy = text => {
    const tmp = document.createElement('textarea')
    tmp.innerText = text
    document.body.appendChild(tmp)
    tmp.select()
    document.execCommand('copy')
    tmp.remove()
}

// eslint-disable-next-line react/prop-types
export const Password = ({ value, index }) => {
    const ref = useRef(null)
    const className = [
        'password',
        'flex items-center px-3',
        `hover:bg-${getColor(index)}-100`,
        'rounded-lg',
        'cursor-pointer',
        'text-xl'
    ].join(' ')

    const onCopy = () =>
        copy(ref.current.innerText)

    return <div className={className}>
        <span className={`text-${getColor(index)}-700 mr-3`}>
            {LETTERS[index]}
        </span>
        <div className='password-value font-light' ref={ref}
            onClick={onCopy}>
            {value}
        </div>
    </div>
}

export const NoDataPlaceholder = () =>
    <div className='flex items-center justify-center text-gray-400 pt-16'>Image placeholder</div>

export const Grid = () => {
    const { key } = useKey()
    const passwords = useMemo(() =>
        generatePasswords(key, LETTERS.length), [key])

    const className = [
        'pt-16',
        'w-full sm:w-3/4 lg:w-1/2 xl:w-1/3 xl:w-1/4',
        'flex flex-col items-center',
        'flex-grow',
        'text-lg bg-white'
    ].join(' ')

    const content = passwords.length > 0
        ? passwords.map((p, i) => <Password key={i} index={i} value={p} />)
        : <NoDataPlaceholder />

    return <div className={className}>
        {content}
    </div>
}

export default Grid