import React from 'react'
import { useState } from 'react'
import { useMemo } from 'react'
import { useCallback } from 'react'
import { useContext } from 'react'
import { createContext } from 'react'

// https://kentcdodds.com/blog/how-to-use-react-context-effectively

const DataContext = createContext({})

const DataProvider = props => {
    const [key, setKey] = useState('')
    const value = useMemo(() => ({ key, setKey }), [key])
    return <DataContext.Provider value={value} {...props} />
}

const useKey = () => {
    const ctx = useContext(DataContext)
    if (Object.keys(ctx).length === 0)
        throw new Error('useKey must be used within a DataProvider')

    const { key, setKey } = ctx
    const clear = useCallback(() => setKey(''), [setKey])
    return { key, setKey, clear }
}

export { useKey, DataProvider }