import ReactDOM from 'react-dom'
import React from 'react'
import './index.css'

import { DataProvider } from '@context'
import { Header } from '@ui'
import { Grid } from '@ui'

const App = () => (
  <DataProvider>
    <Header />
    <Grid />
  </DataProvider>
)

ReactDOM.render(<App />, document.getElementById('app'))
