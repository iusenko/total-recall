const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    // devtool: 'eval-source-map',
    devServer: {
        overlay: true,
        quiet: true,
        port: 3000,
    },

    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        alias: {
            '@context': path.join(__dirname, '/src/context'),
            '@utils': path.join(__dirname, '/src/utils'),
            '@ui': path.join(__dirname, '/src/ui')
        },
        extensions: ['.js']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './web/index.html'
        }),
        new FriendlyErrorsWebpackPlugin()
    ]
}
